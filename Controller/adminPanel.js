const inpFile = document.getElementById("inpFile");
const previewContainer = document.getElementById("imagePreview");
const previewImage = previewContainer.querySelector(".image-preview__image");
const previewDefaultText = previewContainer.querySelector(
  ".image-preview__default-text"
);
var genFile;

inpFile.addEventListener("change", function() {
  const file = this.files[0];

  if (file) {
    const reader = new FileReader();
    genFile = file;
    previewDefaultText.style.display = "none";
    previewImage.style.display = "block";

    reader.addEventListener("load", function() {
      previewImage.setAttribute("src", this.result);
    });
    reader.readAsDataURL(file);
  } else {
    previewDefaultText.style.display = null;
    previewImage.style.display = null;
    previewImage.setAttribute("src", "");
  }
});

var arabaKayitButton = document.getElementById("arabaKayitButton");
arabaKayitButton.setAttribute("onclick", "arabaKayit()");

function arabaKayit() {
  createArabalar();
  var marka = document.getElementById("marka").value;
  var model = document.getElementById("model").value;
  var modelYili = document.getElementById("modelYılı").value;
  var beygir = document.getElementById("beygir").value;
  var fiyat = document.getElementById("fiyat").value;
  var resimAdi = genFile.name;
  var resimYolu = "../img/" + resimAdi + "";

  insertArabalar(marka, model, modelYili, beygir, fiyat, resimYolu)
    .then(function() {
      alert("Araba Eklendi")
    })
    .then(function() {
      location.reload();
    });
}

// var arablariTemizleButton = document.getElementById("arabalariTemizleButton");
// arabalariTemizleButton.setAttribute("onclick", "arablariTemizleFonk()");

// function arablariTemizleFonk() {
//   arabalariTemizle().then(function() {
//     alert("Arabalar Listesini Temizlediniz");
//   });
// }
