
function kullaniciKontrol() {
  anlikHesapListele().then(function(result) {
    var hesapVarmi = result.rows[0];
    if (hesapVarmi == undefined) {
        anasayfaGuestYapilandir();
    } else {
      var girenKullanici = result.rows[0].kullaniciAd;
      if (girenKullanici == "admin") {
       anasayfaAdminYapilandir();
      } else {
        anasayfaCustomerYapilandir()
      }
    }
  });
}

