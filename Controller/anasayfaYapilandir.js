function anasayfaGuestYapilandir() {
  document.getElementById("info").innerHTML =
    "Sipariş Vermek Yada Araba Eklemek İçin Giriş Yapın";
  arabalariListele().then(function(result) {
    var anaRow = document.getElementById("anaRow");
    var len = result.rows.length,
      i;
    for (i = 0; i < len; i++) {
      var div1 = document.createElement("div");
      div1.setAttribute("class", "col-md-4");
      anaRow.appendChild(div1);
      var div2 = document.createElement("div");
      div2.setAttribute("class", "card mb-4 shadow-sm");
      div1.appendChild(div2);
      var img = document.createElement("img");
      img.setAttribute("width", "100%");
      img.setAttribute("height", "225");
      var src = result.rows[i].resimYolu;
      img.setAttribute("src", src);
      div2.appendChild(img);
      var div3 = document.createElement("class", "card-body");
      div3.setAttribute("class", "card-body");
      div2.appendChild(div3);
      var pBilgi = document.createElement("p");
      pBilgi.setAttribute("class", "card-text");
      var bilgi =
        "Marka:" +
        result.rows[i].marka +
        "<br>" +
        "Model:" +
        result.rows[i].model +
        "<br>" +
        "Model Yılı:" +
        result.rows[i].modelYili +
        "<br>" +
        "Beygir:" +
        result.rows[i].beygir +
        "<br>" +
        "Fiyat:" +
        result.rows[i].fiyat;
      pBilgi.innerHTML = bilgi;
      div3.appendChild(pBilgi);
      var div4 = document.createElement("div");
      div4.setAttribute(
        "class",
        "d-flex justify-content-between align-items-center"
      );
      div3.appendChild(div4);
    }
  });
}
function anasayfaCustomerYapilandir() {
  var sagUstButonlar = document.getElementById("sagUstButonlar");
  var girisYapButton = document.getElementById("girisYapButton");
  var kayitButton = document.getElementById("kayitButton");
  girisYapButton.remove();
  kayitButton.remove();
  var cikisButon = document.createElement("button");
  cikisButon.setAttribute("class", "btn btn-outline-success my-2 my-sm-0");
  cikisButon.setAttribute("type", "button");
  cikisButon.setAttribute("id", "cikisYapButton");
  cikisButon.onclick = function() {
    anlikHesapTemizle().then(function() {
      window.location = "../View/Anasayfa.html";
    });
  };
  cikisButon.innerHTML = "Çıkış Yap";
  sagUstButonlar.appendChild(cikisButon);

  var navPanel = document.getElementById("navBarArea");
  navPanel.innerHTML = "SEPET"; //admin panel
  navPanel.setAttribute("href", "Sepet.html");

  arabalariListele().then(function(result) {
    var anaRow = document.getElementById("anaRow");
    var len = result.rows.length,
      i;
    for (i = 0; i < len; i++) {
      var div1 = document.createElement("div");
      div1.setAttribute("class", "col-md-4");
      anaRow.appendChild(div1);
      var div2 = document.createElement("div");
      div2.setAttribute("class", "card mb-4 shadow-sm");
      div1.appendChild(div2);
      var img = document.createElement("img");
      img.setAttribute("width", "100%");
      img.setAttribute("height", "225");
      var src = result.rows[i].resimYolu;
      img.setAttribute("src", src);
      div2.appendChild(img);
      var div3 = document.createElement("class", "card-body");
      div3.setAttribute("class", "card-body");
      div2.appendChild(div3);
      var pBilgi = document.createElement("p");
      pBilgi.setAttribute("class", "card-text");
      var bilgi =
        "Marka:" +
        result.rows[i].marka +
        "<br>" +
        "Model:" +
        result.rows[i].model +
        "<br>" +
        "Model Yılı:" +
        result.rows[i].modelYili +
        "<br>" +
        "Beygir:" +
        result.rows[i].beygir +
        "<br>" +
        "Fiyat:" +
        result.rows[i].fiyat;
      pBilgi.innerHTML = bilgi;
      div3.appendChild(pBilgi);
      var div4 = document.createElement("div");
      div4.setAttribute(
        "class",
        "d-flex justify-content-between align-items-center"
      );
      div3.appendChild(div4);
      var div5 = document.createElement("div");
      div5.setAttribute("class", "btn-group");
      div4.appendChild(div5);
      var sepeteEkleBut = document.createElement("button");
      sepeteEkleBut.setAttribute("class", "btn btn-sm btn-outline-secondary");
      sepeteEkleBut.setAttribute(
        "onclick",
        "sepeteEkle(" + result.rows[i].id + ");"
      );
      sepeteEkleBut.innerHTML = "Sepete Ekle";
      div5.appendChild(sepeteEkleBut);
    }
  });
}
function anasayfaAdminYapilandir() {
  var sagUstButonlar = document.getElementById("sagUstButonlar");
  var girisYapButton = document.getElementById("girisYapButton");
  var kayitButton = document.getElementById("kayitButton");
  girisYapButton.remove();
  kayitButton.remove();
  var cikisButon = document.createElement("button");
  cikisButon.setAttribute("class", "btn btn-outline-success my-2 my-sm-0");
  cikisButon.setAttribute("type", "button");
  cikisButon.setAttribute("id", "cikisYapButton");
  cikisButon.onclick = function() {
    anlikHesapTemizle().then(function() {
      window.location = "../View/Anasayfa.html";
    });
  };
  cikisButon.innerHTML = "Çıkış Yap";
  sagUstButonlar.appendChild(cikisButon);

  var navPanel = document.getElementById("navBarArea");
  navPanel.innerHTML = "Yeni Araba Ekle"; //admin panel
  navPanel.setAttribute("href", "AdminPanel.html");

  arabalariListele().then(function(result) {
    var anaRow = document.getElementById("anaRow");
    var len = result.rows.length,
      i;
    for (i = 0; i < len; i++) {
      var div1 = document.createElement("div");
      div1.setAttribute("class", "col-md-4");
      anaRow.appendChild(div1);
      var div2 = document.createElement("div");
      div2.setAttribute("class", "card mb-4 shadow-sm");
      div1.appendChild(div2);
      var img = document.createElement("img");
      img.setAttribute("width", "100%");
      img.setAttribute("height", "225");
      var src = result.rows[i].resimYolu;
      img.setAttribute("src", src);
      div2.appendChild(img);
      var div3 = document.createElement("class", "card-body");
      div3.setAttribute("class", "card-body");
      div2.appendChild(div3);
      var pBilgi = document.createElement("p");
      pBilgi.setAttribute("class", "card-text");
      var bilgi =
        "Marka:" +
        result.rows[i].marka +
        "<br>" +
        "Model:" +
        result.rows[i].model +
        "<br>" +
        "Model Yılı:" +
        result.rows[i].modelYili +
        "<br>" +
        "Beygir:" +
        result.rows[i].beygir +
        "<br>" +
        "Fiyat:" +
        result.rows[i].fiyat;
      pBilgi.innerHTML = bilgi;
      div3.appendChild(pBilgi);
      var div4 = document.createElement("div");
      div4.setAttribute(
        "class",
        "d-flex justify-content-between align-items-center"
      );
      div3.appendChild(div4);
      var div5 = document.createElement("div");
      div5.setAttribute("class", "btn-group");
      div4.appendChild(div5);
      var duzBut = document.createElement("button");
      duzBut.setAttribute("class", "btn btn-sm btn-outline-secondary");
      duzBut.setAttribute(
        "onclick",
        "arabaDuzenle(" + result.rows[i].id + ");"
      );
      duzBut.innerHTML = "Düzenle";
      div5.appendChild(duzBut);
      var silBut = document.createElement("button");
      silBut.setAttribute("class", "btn btn-sm btn-outline-secondary");
      silBut.setAttribute("onclick", "arabaSil(" + result.rows[i].id + ");");
      silBut.innerHTML = "Sil";
      div5.appendChild(silBut);
    }
  });
}

function arabaSil(id) {
  arabayiSil(id)
    .then(function() {
      alert("Araba Silindi");
    })
    .then(function() {
      location.reload();
    });
}
