function arabaDuzenle(id) {
  arabayiGetir(id)
    .then(function(result) {
      localStorage.setItem("marka", result.rows[0].marka);
      localStorage.setItem("model", result.rows[0].model);
      localStorage.setItem("modeyYili", result.rows[0].modelYili);
      localStorage.setItem("beygir", result.rows[0].beygir);
      localStorage.setItem("fiyat", result.rows[0].fiyat);
      localStorage.setItem("resimYolu", result.rows[0].resimYolu);
      localStorage.setItem("id", result.rows[0].id);
    })
    .then(function() {
      window.location = "../View/ArabaDuzenle.html";
    });
}

function inputDoldur() {
  var duzenleMarka = document.getElementById("duzenleMarka");
  var duzenleModel = document.getElementById("duzenleModel");
  var duzenleModelYili = document.getElementById("duzenleModelYili");
  var duzenleBeygir = document.getElementById("duzenleBeygir");
  var duzenleFiyat = document.getElementById("duzenleFiyat");
  var duzenleImg = document.getElementById("duzenleImg");

  duzenleMarka.value = localStorage.getItem("marka");
  duzenleModel.value = localStorage.getItem("model");
  duzenleModelYili.value = localStorage.getItem("modeyYili");
  duzenleBeygir.value = localStorage.getItem("beygir");
  duzenleFiyat.value = localStorage.getItem("fiyat");
  duzenleImg.src = localStorage.getItem("resimYolu");
}
const inpFile = document.getElementById("inpFileD");
const previewContainer = document.getElementById("imagePreviewD");
var previewImage = document.getElementById("duzenleImg");
var genFile;

inpFile.addEventListener("change", function() {
  const file = this.files[0];

  if (file) {
    const reader = new FileReader();
    genFile = file;
    previewImage.style.display = "block";

    reader.addEventListener("load", function() {
      previewImage.setAttribute("src", this.result);
    });
    reader.readAsDataURL(file);
  }
});

var arabaDuzenleButton = document.getElementById("arabaDuzenleButton");
arabaDuzenleButton.setAttribute("onclick", "arabayiDuzenle()");

function arabayiDuzenle() {
  var tempId = localStorage.getItem("id");
  var duzenleMarka = document.getElementById("duzenleMarka").value;
  var duzenleModel = document.getElementById("duzenleModel").value;
  var duzenleModelYili = document.getElementById("duzenleModelYili").value;
  var duzenleBeygir = document.getElementById("duzenleBeygir").value;
  var duzenleFiyat = document.getElementById("duzenleFiyat").value;
  var resimAdi = genFile.name;
  var resimYolu = "../img/" + resimAdi + "";

  arabayiGuncelle(
    duzenleMarka,
    duzenleModel,
    duzenleModelYili,
    duzenleBeygir,
    duzenleFiyat,
    resimYolu,
    tempId
  ).then(function() {
    alert("Araba güncellendi");
  });
}
