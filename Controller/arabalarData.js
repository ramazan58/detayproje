var dbArabalar = openDatabase(
  "dbArabalar",
  "1.0",
  "Arabaların kayıtlarının tutulduğu listeler",
  2 * 1024 * 1024
);

function createArabalar() {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS ARABALAR (id INTEGER PRIMARY KEY,marka VARCHAR(100),model VARCHAR(100),modelYili VARCHAR(100),beygir VARCHAR(100),fiyat VARCHAR(100),resimYolu VARCHAR(100))"
      );
      resolve();
    });
  });
}

function arabayiGuncelle(
  marka,
  model,
  modelYili,
  beygir,
  fiyat,
  resimYolu,
  arabaId
) {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql(
        "UPDATE ARABALAR SET marka=?,model=?,modelYili=? ,beygir=? ,fiyat=? ,resimYolu=? WHERE id=?",
        [marka, model, modelYili, beygir, fiyat, resimYolu, arabaId]
      );
      resolve();
    });
  });
}

function insertArabalar(marka, model, modelYili, beygir, fiyat, resimYolu) {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql(
        "INSERT INTO ARABALAR (marka, model, modelYili,beygir,fiyat,resimYolu) VALUES (?,?,?,?,?,?)",
        [marka, model, modelYili, beygir, fiyat, resimYolu]
      );
      resolve();
    });
  });
}

function arabalariListele() {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql(
        "SELECT * FROM ARABALAR",
        [],
        function(tx, results) {
          resolve(results);
        },
        null
      );
    });
  });
}
function arabayiGetir(id) {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql(
        "SELECT * FROM ARABALAR WHERE id = " + id + "",
        [],
        function(tx, results) {
          resolve(results);
        },
        null
      );
    });
  });
}

function arabalariTemizle() {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql("DELETE FROM ARABALAR");
      resolve();
    });
  });
}

function arabayiSil(id) {
  return new Promise(function(resolve, reject) {
    dbArabalar.transaction(function(tx) {
      tx.executeSql("DELETE FROM ARABALAR WHERE id = " + id + " ");
      resolve();
    });
  });
}
