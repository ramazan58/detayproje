var dbHesaplar = openDatabase(
    "dbHesaplar",
    "1.0",
    "Hesapların kayıtlarının tutulduğu listeler",
    2 * 1024 * 1024
  );
  
  function createHesaplar() {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS HESAPLAR (id INTEGER PRIMARY KEY,ad VARCHAR(100),soyad VARCHAR(100),kullaniciAd VARCHAR(100),parola VARCHAR(100))"
        );
        resolve();
      });
    });
  }
  
  function insertHesaplar(ad, soyad, kullaniciAd,parola) {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql(
          "INSERT INTO HESAPLAR (ad,soyad,kullaniciAd,parola) VALUES (?,?,?,?)",
          [ad, soyad, kullaniciAd,parola]
        );
        resolve();
      });
    });
  }
  
  function hesaplariListele() {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql(
          "SELECT * FROM HESAPLAR",
          [],
          function(tx, results) {
            resolve(results);
          },
          null
        );
      });
    });
  }
  
  function hesaplariTemizle() {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql("DELETE FROM HESAPLAR");
        resolve();
      });
    });
  }
  
  function hesapSil(id) {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql("DELETE FROM HESAPLAR WHERE id = " + id + " ");
        resolve();
      });
    });
  }

  function createAnlıkHesap() {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS ANLIKHESAP (id INTEGER PRIMARY KEY,ad VARCHAR(100),soyad VARCHAR(100),kullaniciAd VARCHAR(100),parola VARCHAR(100))"
        );
        resolve();
      });
    });
  }
  function insertAnlıkHesap(ad, soyad, kullaniciAd,parola) {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql(
          "INSERT INTO ANLIKHESAP (ad,soyad,kullaniciAd,parola) VALUES (?,?,?,?)",
          [ad, soyad, kullaniciAd,parola]
        );
        resolve();
      });
    });
  }
  function anlikHesapTemizle() {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql("DELETE FROM ANLIKHESAP");
        resolve();
      });
    });
  }

  function anlikHesapListele() {
    return new Promise(function(resolve, reject) {
      dbHesaplar.transaction(function(tx) {
        tx.executeSql(
          "SELECT * FROM ANLIKHESAP",
          [],
          function(tx, results) {
            resolve(results);
          },
          null
        );
      });
    });
  }


//   function hesapGuncelle(inputTarih, selectOgun, inputYemek, trid) {
//     return new Promise(function(resolve, reject) {
//       dblist.transaction(function(tx) {
//         tx.executeSql("UPDATE YEMEKLISTE SET tarih=?,ogun=?,yemek=? WHERE id=?", [
//           inputTarih,
//           selectOgun,
//           inputYemek,
//           trid
//         ]);
//         resolve()
//       });
//     });
//   }
  