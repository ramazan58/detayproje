function sepeteEkle(id) {
  createSepet();
  var marka, model, modelYili, beygir, fiyat, resimYolu;
  arabayiGetir(id).then(function(result) {
    marka = result.rows[0].marka;
    model = result.rows[0].model;
    modelYili = result.rows[0].modelYili;
    beygir = result.rows[0].beygir;
    fiyat = result.rows[0].fiyat;
    resimYolu = result.rows[0].resimYolu;
    insertSepet(marka, model, modelYili, beygir, fiyat, resimYolu).then(
      alert("Araba sepete eklendi")
    );
  });
}

function sepetiDoldur() {
  var sepetListTable = document.getElementById("sepetListTable");
  sepetiListele().then(function(result) {
    var len = result.rows.length,
      i;
    for (i = 0; i < len; i++) {
      var tr = document.createElement("tr");
      sepetListTable.appendChild(tr);
      var img = document.createElement("img");
      img.src = result.rows[i].resimYolu;
      //img.setAttribute("src",);
      img.setAttribute("width", "170px");
      img.setAttribute("height", "100px");
      var tdimg = document.createElement("td");
      tdimg.style.width = "180px";
      tdimg.appendChild(img);
      tr.appendChild(tdimg);
      var tdmarka = document.createElement("td");
      tdmarka.style.width = "150px";
      var tdmodel = document.createElement("td");
      var tdmodelYili = document.createElement("td");
      var tdbeygir = document.createElement("td");
      var tdfiyat = document.createElement("td");
      tdfiyat.style.width = "130px";
      tdmarka.innerHTML = result.rows[i].marka;
      tdmodel.innerHTML = result.rows[i].model;
      tdmodelYili.innerHTML = result.rows[i].modelYili;
      tdbeygir.innerHTML = result.rows[i].beygir;
      tdfiyat.innerHTML = result.rows[i].fiyat;
      tr.appendChild(tdmarka);
      tr.appendChild(tdmodel);
      tr.appendChild(tdmodelYili);
      tr.appendChild(tdbeygir);
      tr.appendChild(tdfiyat);

      var tdsil = document.createElement("td");
      tdsil.style.width = "140px";
      tr.appendChild(tdsil);
      var silButon = document.createElement("button");
      silButon.setAttribute("class", "btn btn-danger");
      silButon.innerHTML = "SİL";
      tdsil.appendChild(silButon);
      silButon.setAttribute(
        "onclick",
        "sepettekiniSil(" + result.rows[i].id + ")"
      );
    }
  });
}
function sepettekiniSil(id) {
  sepetteSil(id)
    .then(function() {
      alert("Arabayı Sepetten Kaldırdınız");
    })
    .then(function() {
      location.reload();
    });
}
