var dbSepet = openDatabase(
  "dbSepettekiler",
  "1.0",
  "Sepetteki Ürülerin tutulduğu yer",
  2 * 1024 * 1024
);

function createSepet() {
  return new Promise(function(resolve, reject) {
    dbSepet.transaction(function(tx) {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS SEPET (id INTEGER PRIMARY KEY,marka VARCHAR(100),model VARCHAR(100),modelYili VARCHAR(100),beygir VARCHAR(100),fiyat VARCHAR(100),resimYolu VARCHAR(100))"
      );
      resolve();
    });
  });
}

function insertSepet(marka, model, modelYili, beygir, fiyat, resimYolu) {
  return new Promise(function(resolve, reject) {
    dbSepet.transaction(function(tx) {
      tx.executeSql(
        "INSERT INTO SEPET (marka,model,modelYili,beygir,fiyat,resimYolu) VALUES (?,?,?,?,?,?)",
        [marka, model, modelYili, beygir, fiyat, resimYolu]
      );
      resolve();
    });
  });
}

function sepetiListele() {
  return new Promise(function(resolve, reject) {
    dbSepet.transaction(function(tx) {
      tx.executeSql(
        "SELECT * FROM SEPET",
        [],
        function(tx, results) {
          resolve(results);
        },
        null
      );
    });
  });
}

function sepetiTemizle() {
  return new Promise(function(resolve, reject) {
    dbSepet.transaction(function(tx) {
      tx.executeSql("DELETE FROM SEPET");
      resolve();
    });
  });
}

function sepetteSil(id) {
  return new Promise(function(resolve, reject) {
    dbSepet.transaction(function(tx) {
      tx.executeSql("DELETE FROM SEPET WHERE id = " + id + " ");
      resolve();
    });
  });
}
